/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     25/08/2022 1:47:44 p. m.                     */
/*==============================================================*/


/*==============================================================*/
/* Table: categorias                                            */
/*==============================================================*/
create table categorias
(
   idcategoria          bigint not null auto_increment  comment '',
   nombrecategoria      varchar(100) not null  comment '',
   primary key (idcategoria)
);

/*==============================================================*/
/* Table: productos                                             */
/*==============================================================*/
create table productos
(
   idproducto           bigint not null auto_increment  comment '',
   idcategoria          bigint not null  comment '',
   nombreproducto       varchar(100) not null  comment '',
   referenciaproducto   varchar(20) not null  comment '',
   preciocostoproducto  numeric(12,2) not null  comment '',
   precioventaproducto  numeric(12,2) not null  comment '',
   cantidadproducto     float not null  comment '',
   primary key (idproducto)
);

alter table productos add constraint fk_producto_reference_categori foreign key (idcategoria)
      references categorias (idcategoria) on delete cascade on update cascade;

